# LDC module

## Overview

The LDC (Lens Distortion Correction) module deals with lens geometric distortion issues in the camera system. The distortion can be common optical distortions, such as barrel distortion, pin-cushion distortion, or fisheye distortion. It aslo support affine transformation and perspective transformations.

## Supported Features

**1. Support up to `8192 x 8192` image dimension**

**2. Pixel Interpolation**
  * Bi-cubic interpolation for Y and bilinear interpolation for Cb/Cr
  * Bilinear interpolation mode to offer double throughput

**3. Support input**
  * NV12
  * UYVY
  * 8bit Luma only
  * 12bit in 16bit container Luma only or 12bit packed Luma only

**4. Support ouput**
  * NV12
  * UYVY
  * YUYV
  * bit Luma only
  * 12bit in 16bit container Luma only or 12bit packed Luma only

**5. Support for independent block size in multiple regions (up to 9 regions)**

**6. MECC support on Mesh Data internal storage**

## Functional Description

### Affine Transform

### Perspective Transformation

### Lens Distortion Back Mapping

#### Mathematical Lens Model

* Equisolid projection
``` math
r_d = 2f \cdot \sin(\frac{\theta}{2})
```

* Pinhole perspective projection
``` math
r_u = \frac{f}{s}  \cdot \tan(\theta)
```

* Mesh LUT
``` math
r_u = \sqrt{(h_p - h_c)^2 + (v_p - v_c)^2}

\theta = \arctan(\frac{r_u \cdot s}{f})

f_c = \frac{r_d}{r_u} = s \cdot \frac{\cos(\theta)}{\cos(\frac{\theta}{2})}
```
then
``` math
\left[
\begin{matrix}
 h_p \\
 v_p \\
\end{matrix}
\right] = 
\left[
\begin{matrix}
 h_c \\
 v_c \\
\end{matrix}
\right] + 
\left[
\begin{matrix}
 h_p - h_c \\
 v_p - v_c \\
\end{matrix}
\right] \cdot f_c
```

* Mesh table

Distortion correction is specified by offsets stored in a mesh table as below:
``` math
\left[
\begin{matrix}
 \Delta h \\
 \Delta v \\
\end{matrix}
\right] =
\left[
\begin{matrix}
 h_d \\
 v_d \\
\end{matrix}
\right] -
\left[
\begin{matrix}
 h_p \\
 v_p \\
\end{matrix}
\right]
```
``` note::
  (Δh, Δv): S16Q3 format (signed number on 16 bits with 13 bits for integer and 3 bits of fraction)

  (hd, vd): U16Q3 format (unsigned number on 16 bits with 13 bits for integer and 3 bits of fraction)

  (hp, vp): U16Q3 format (unsigned number on 16 bits with 13 bits for integer and 3 bits of fraction)
```
#### Mesh table generation

According to the [LDC.pdf](#references) from TI, it's easy to implement the program of [mesh table generator](https://recommonmark.readthedocs.io/en/latest/auto_structify.html#inline-math).

##### Runtime environment setup

1. Octave installing
```
$ sudo apt-add-repository ppa:octave/stable
$ sudo apt-get update
$ sudo apt-get install octave
```

2. Python dependent packages installing
```
$ pip install --user numpy matplotlib pandas PySide2
```

##### Usage

``` sidebar:: Input parameters

     From Lens Model to Mesh LUT:
       Only need reference picture.
     From Lens Specification to Mesh LUT:
       Need lens specification and reference picture.
```

```
$ ./mesh-gen [model]
```

#### Example

The [sample code](https://scm-gitlab.com/) for ldc module is base on OpenVX frameware. It read a bmp file and a mesh table as input, then save a undistorted bmp file as output.

#### OpenVX Interface

The OpenVX Interface for ldc node see [tivxVpacLdcNode](file:///E:/workspace/ipu02/psdkra/tiovx/docs/user_guide/group__group__vision__function__vpac__ldc.html#gade2239f717b1460e12be52499c6d3b4c).

##### Usage
```
$ cd /opt/vision_sdk
$ ./vx_app_ldc_test --cfg ./app.cfg
```

## References
1. Mesh Lens Distortion Correction `\\hzhe003a\dfs\dida3091\`
2. Technical Reference Manual `\\hzhe003a\dfs\dida3091\`
.. _rst-index:

=====================
Hardware Accelerators
=====================

This chapter describes the Vision Accelerators in TDA4X SOC.
Include Vision Pre-processing Accelerator (VPAC) and
the Depth and Motion Perception Accelerator (DMPAC).

.. toctree::
   :maxdepth: 2

   ldc_module.md

.. develop documentation master file, created by
   sphinx-quickstart on Mon Feb  3 23:40:09 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _contents:

Welcome to IPU02 documentation!
===================================

.. toctree::
    :caption: Table of Contents
    :name: _ipu02root
    :hidden:
    :numbered:

    usage/installation
    usage/quickstart

    hwmodules/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
